# MaLo24_testcases

**(Ich selber bin nicht vom Lehrstuhl und _höre_ Malo nur)**

Das Teilen von Testcases ist erlaubt, solange kein Teil der Lösung geteilt wird [source](https://moodle.rwth-aachen.de/mod/hsuforum/discuss.php?d=6602)

Bei Fragen, Anmerkungen, Hinweisen auf Fehler oder falls ihr weitere Testcases habt: luis.matzke@rwth-aachen.de

Ihr könnt das repo auch forken Tests hinzufügen und anschließend mergerequests stellen.
## Pytest
Im Repo findet ihr eine test_assignment.py, damit werden automatisch alle custom_tests erkannt 
und ihr könnt ganz normal pytest als Testrunner nutzen.
Ihr müsst nur die jeweiligen funktionen der Woche importen

## Abgabe 7
### prenex_normal_form
```py
from util import custom_test
def _equal(f1: Formula, f2: Formula) -> bool:
    """Returns True when the formulas are equal, ignoring differing names of bound variables"""
    if f1.free_variables != f2.free_variables: return False
    if type(f1) != type(f2): return False
    if isinstance(f1, Quantifier) and isinstance(f2, Quantifier):
        if f1.variable == f2.variable: return _equal(f1.subformula, f2.subformula)
        if f1.variable != f2.variable:
            return _equal(f1.subformula, f2.subformula.substitute(f2.variable, f1.variable))
    return bool(f1 == f2)

def _equal_to(f1: Formula):
    return lambda f2: _equal(f1, f2)


c, f, g = FunctionSymbol("C", 0), FunctionSymbol("F", 1), FunctionSymbol("G", 2)
x, y = Variable("x"), Variable("y")
z1, z2 = Variable("z1"), Variable("z2")
e = RelationSymbol("E", 2)

#@custom_test((f1), equal_to(f2))
@custom_test((And(Exists(x, e(x, y)), Exists(y, e(x, y)))), _equal_to(Exists(z1, Exists(z2, e(z1, y) & e(x, z2)))))
@custom_test((Forall(x, Neg(Impl(Exists(y, e(x,y)), Exists(x, e(x,y)))))), _equal_to(Forall(x, Exists(z1, Forall(z2, e(x, z1) & Neg(e(z2, y)))))))
@custom_test((Impl(Exists(x, e(x,x)), Forall(x, e(x,c)))), _equal_to(Forall(x, Forall(z1, Neg(e(x,x)) | e(z1, c)))))
```

## Abgabe 6

### substitute_term

```py
f, g, h, c = FunctionSymbol("F", 2), FunctionSymbol("G", 2), FunctionSymbol("H", 1), FunctionSymbol("C", 0)
x, a, b = Variable("x"), Variable("a"), Variable("b")

@custom_test((f(x, g(a, b)), x, h(c)), f(h(c), g(a, b)))
```

### substitute_formula

```py
f, g, h, c = FunctionSymbol("F", 2), FunctionSymbol("G", 2), FunctionSymbol("H", 1), FunctionSymbol("C", 0)
x, a, b = Variable("x"), Variable("a"), Variable("b")
r, p, yep = RelationSymbol("R", 2), RelationSymbol("P", 1), TruthConstant(True)

@custom_test((Equality(x, a), x, h(c)), Equality(h(c), a))
@custom_test((Exists(x, r(x, b)), x, h(c)), Exists(x, r(x, b)))
@custom_test((Exists(x, r(x, b)), b, h(c)), Exists(x, r(x, h(c))))
@custom_test((Forall(x, r(x, b)), b, b), Forall(x, r(x, b)))
@custom_test((And(Or(Neg(r(x, a)), Impl(Equality(f(a, b), x), r(h(c), c))), yep), x, g(a, b)), And(Or(Neg(r(g(a, b), a)), Impl(Equality(f(a, b), g(a, b)), r(h(c), c))), yep))
@custom_test((And(Or(Neg(r(x, a)), Impl(Forall(x, r(x, b)), r(h(c), c))), yep), x, g(a, b)), And(Or(Neg(r(g(a, b), a)), Impl(Forall(x, r(x, b)), r(h(c), c))), yep))
@custom_test((Neg(Equality(a, b)), a, f(c, a)), Neg(Equality(f(c, a), b)))
# FIXED! quantor conflicts (see code-task description on assignment not docstring)
@custom_test(( Exists(a,p(h(b))), b, h(a) ), lambda out: out==Exists(out.variable, p(h(h(a)))) and out.variable not in [a,b] )
@custom_test(( Exists(a, p(g(b,a))),b,h(a)), lambda out: out==Exists(out.variable, p(g(h(a),out.variable))) and out.variable not in [a,b] )
```

## Abgabe 5

### evaluate_term

```py
f, c, x = FunctionSymbol("F", 1), FunctionSymbol("C", 0), Variable("x")
g = FunctionSymbol("G", 2)
some_interpretation = Interpretation(
    Structure(range(10), {f: lambda n: n + 1, c: 5, g: lambda x, y: x - y}),
    {x: 3},
)

@custom_test((x, some_interpretation), 3)
@custom_test((f(x), some_interpretation), 4)
@custom_test((c(), some_interpretation), 5)
@custom_test((g(x,x), some_interpretation), 0)
@custom_test((f(g(c(), f(x))), some_interpretation), 2)
```

### evaluate_atom

```py
f, c, x, = FunctionSymbol("F", 1), FunctionSymbol("C", 0), Variable("x")
g = FunctionSymbol("G", 2)
y, z = Variable("y"), Variable("z")
r = RelationSymbol("R", 2)
t, nt = TruthConstant(True), TruthConstant(False)
some_interpretation = Interpretation(
    Structure(range(10), {f: lambda x: x + 1, c: 5, g: lambda x, y: x - y, r: lambda x, y: x < y}),
    {x: 3, y: 5, z: 7},
)

@custom_test((f(x) == c, some_interpretation), False)
@custom_test((f(f(x)) == c, some_interpretation), True)
@custom_test((f(x) == f(x), some_interpretation), True)
@custom_test((f(x) == f(g(c(), f(x))), some_interpretation), False)
@custom_test((f(x) == f(g(c(), f(g(c(), f(x))))), some_interpretation), True)
@custom_test((r(x, y), some_interpretation), True)
@custom_test((r(x, c()), some_interpretation), True)
@custom_test((r(z, y), some_interpretation), False)
@custom_test((r(f(f(f(x))), y), some_interpretation), False)
@custom_test((r(z, f(f(f(x)))), some_interpretation), False)
@custom_test((r(z, f(f(f(f(f(x)))))), some_interpretation), True)
@custom_test((t, some_interpretation), True)
@custom_test((nt, some_interpretation), False)
```

## Abgabe 4

### choose_literal

```py
X = Symbol("X")
Y = Symbol("Y")
@custom_test(BigAnd(BigOr(X)), X, "positive literal") # (X) initial
@custom_test(BigAnd(BigOr(Neg(X))), Neg(X), "negative literal") 
@custom_test(BigAnd(BigOr(Neg(X), Y)), [Y, Neg(X)])
@custom_test(BigAnd(BigOr(Y), BigOr(Neg(X))), [Y, Neg(X)])
```

### dpll

```py
X = Symbol("X")
Y = Symbol("Y")

# initial test
# (X /\ ~X)
@custom_test(BigAnd(BigOr(X), BigOr(~X)), None)

#
# ((X \/ ~X) /\ (X \/ ~X))
@custom_test(
    BigAnd(BigOr(Symbol("X"),Neg(Symbol("X"))),BigOr(Symbol("X"),Neg(Symbol("X")))),
    [{Symbol(label='X'): True},{Symbol(label='X'): False}]
)

# Testing multiple empty klauseln
# (X /\ ~X /\ ~X /\ X)
@custom_test( BigAnd( BigOr(Symbol("X")), BigOr(Neg(Symbol("X"))), BigOr(Neg(Symbol("X"))),BigOr(Symbol("X")) ), None )

# Formula from Lecture
# Test verifies by checking if truth table evaluates the formula to true
# Therefore less strict but more edge cases.
# ((P1 \/ ~P5 \/ ~P6 \/ P7) /\ (~P1 \/ P2 \/ ~P5) /\ (~P1 \/ ~P2 \/ ~P3 \/ ~P5 \/ ~P6) /\ (P1 \/ P2 \/ ~P4 \/ P7) /\ (~P4 \/ ~P6 \/ ~P7) /\ (P3 \/ ~P5 \/ P7) /\ (P3 \/ ~P4 \/ ~P5) /\ (P5 \/ ~P6) /\ (P5 \/ P4 \/ ~P8) /\ (P1 \/ P3 \/ P5 \/ P6 \/ P7) /\ (~P7 \/ P8) /\ (~P6 \/ ~P7 \/ ~P8))
@custom_test(inp := BigAnd([
    BigOr(Symbol("P1"),Neg(Symbol("P5")),Neg(Symbol("P6")),Symbol("P7")),
    BigOr(Neg(Symbol("P1")),Symbol("P2"),Neg(Symbol("P5"))),
    BigOr(Neg(Symbol("P1")),Neg(Symbol("P2")),Neg(Symbol("P3")),Neg(Symbol("P5")),Neg(Symbol("P6"))),
    BigOr(Symbol("P1"),Symbol("P2"),Neg(Symbol("P4")),Symbol("P7")),
    BigOr(Neg(Symbol("P4")),Neg(Symbol("P6")),Neg(Symbol("P7"))),
    BigOr(Symbol("P3"),Neg(Symbol("P5")),Symbol("P7")),
    BigOr(Symbol("P3"),Neg(Symbol("P4")),Neg(Symbol("P5"))),
    BigOr(Symbol("P5"),Neg(Symbol("P6"))),
    BigOr(Symbol("P5"),Symbol("P4"),Neg(Symbol("P8"))),
    BigOr(Symbol("P1"),Symbol("P3"),Symbol("P5"),Symbol("P6"),Symbol("P7")),
    BigOr(Neg(Symbol("P7")),Symbol("P8")),
    BigOr(Neg(Symbol("P6")),Neg(Symbol("P7")),Neg(Symbol("P8")))
    ]),
    lambda out: inp.evaluate( out ),
    "DPLL example formula from lecture"
)

# ((~Q \/ ~P \/ ~T \/ R) /\ P /\ S /\ (~R \/ ~Q \/ ~P) /\ T /\ (Q \/ ~P \/ ~T))
@custom_test(BigAnd([
    BigOr(Neg(Symbol("Q")),Neg(Symbol("P")),Neg(Symbol("T")),Symbol("R")),
    BigOr(Symbol("P")),
    BigOr(Symbol("S")),
    BigOr(Neg(Symbol("R")),Neg(Symbol("Q")),Neg(Symbol("P"))),
    BigOr(Symbol("T")),
    BigOr(Symbol("Q"),Neg(Symbol("P")),Neg(Symbol("T")))
]), None )

# ((R \/ ~P \/ ~S) /\ R /\ (S \/ ~R \/ ~Q) /\ P /\ (~R \/ P \/ ~T) /\ (~R \/ S \/ ~P))
@custom_test(inp2 := BigAnd([
    BigOr(Symbol("R"),Neg(Symbol("P")),Neg(Symbol("S"))),
    BigOr(Symbol("R")),
    BigOr(Symbol("S"),Neg(Symbol("R")),Neg(Symbol("Q"))),
    BigOr(Symbol("P")),
    BigOr(Neg(Symbol("R")),Symbol("P"),Neg(Symbol("T"))),
    BigOr(Neg(Symbol("R")),Symbol("S"),Neg(Symbol("P")))
    ]),
    lambda out: inp2.evaluate( out )
)

@custom_test(BigAnd(
    BigOr(X,Y),
    BigOr(~X,~Y),
    BigOr(X,~Y),
    BigOr(~X,Y)
), None, "Algo l. 12")
```
